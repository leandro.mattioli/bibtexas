from bibtexparser.bparser import BibTexParser
from bibtexparser.bwriter import BibTexWriter
from bibtexparser.bibdatabase import BibDatabase
import bibtexparser
from doi2bib.crossref import get_bib 
from habanero import Crossref
import time
from datetime import datetime
from os import path

cr = Crossref()


def load_bib(bibpath):
    """Parse a BibTeX file""" 
    parser = BibTexParser(common_strings=True)
    parser.ignore_nonstandard_types = False
    with open(bibpath, 'r', encoding='utf8') as fd:
        db = bibtexparser.load(fd, parser=parser)
    return db


def load_or_create(bibpath):
    """Load a database or create a new one if the file doesn't exist"""
    db = None
    if path.isfile(bibpath):
        db = load_bib(bibpath)
    if not db:
        db = BibDatabase()
    return db


def find_by_doi(database, doi, case_sensitive=False):
    """Filter a database by DOI"""
    if not case_sensitive:
        return [x for x in database.entries if x['doi'].lower() == doi.lower()]
    else:
        return [x for x in database.entries if x['doi'] == doi]


def fetch_by_doi(doi, browser=None):
    """Try to retrieve a BibTeX record string by a DOI identifier"""
    result, bibtex = get_bib(doi)
    if result:
        return bibtex
    if browser:
        html_entry = browser.find_by_css('input.form-control').first
        html_button = browser.find_by_css('button.btn').first
        html_entry.fill(doi)
        html_button.click()
        time.sleep(5)
        html_result = browser.find_by_css('.bibtex-code')
        if not html_result:
            return False
        return html_result.first.text


def fetch_abstract(dois):
    """Fetch abstract from DOI"""
    response = cr.works(ids=dois)
    if response['status'] == 'ok':
        if 'abstract' in response['message']:
            return response['message']['abstract']
    return False


def entry_doi(entry):
    """Gets the DOi from a bibliography entry dictionary"""
    # Common column names BibTeX, IEEE CS, SpringerLink
    alternatives = ['doi', 'DOI', 'Item DOI']
    for alt in alternatives:
        if alt in entry:
            return entry[alt]
    return False

def is_recent(bibentry, oldest):
    if 'year' in bibentry:
        return int(bibentry['year']) > oldest
    elif 'date' in bibentry:
        return int(bibentry['date']) > oldest
    return False

def filter_recent(database, n=5):
    """Filter a database returning a database with only recent entries"""
    curr_year = datetime.now().year
    oldest = curr_year - 5
    filtered_database = BibDatabase()
    filtered_database.entries = [ x for x in database.entries if \
                                  is_recent(x, oldest) ]
    return filtered_database
